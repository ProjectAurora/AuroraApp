﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Aurora
{
    public class AchievementsManager : MonoBehaviour
    {
        public MessageBox messageBox;
        public Text levelText;
        public Text scoreText;
        public Text playerNameText;
        public Image[] icons;
        public Text[] titles;
        public string[] descriptions;

        public void ShowDescription(int index)
        {
            messageBox.Show(AchievementResources.Instance.achievementInfo[index].title, descriptions[index]);
        }

        public void RefreshScore()
        {
            Task<Player> queryPlayer = CloudServices.Instance.QueryCurrentPlayer();
            queryPlayer.ContinueWith(t =>
            {

                if (!t.IsFaulted)
                {
                    scoreText.text = t.Result.totalPoints.ToString();
                    GSui.Instance.MoveIn(scoreText.transform, true);
                    levelText.text = t.Result.currentLevel.ToString();
                    GSui.Instance.MoveIn(levelText.transform, true);
                    playerNameText.text = t.Result.name;
                    GSui.Instance.MoveIn(playerNameText.transform, true);

                    descriptions = new string[AchievementResources.Instance.achievementInfo.Length];

                    for (int i = 0; i < AchievementResources.Instance.achievementInfo.Length; ++i)
                    {
                        AchievementResources.Info info = AchievementResources.Instance.achievementInfo[i];
                        bool isUnlocked = t.Result.unlockedAchievementIds.Contains(info.id);
                        if(isUnlocked)
                        {
                            Debug.Log("Show info for: " + info.id);
                            titles[i].text = info.title;
                            descriptions[i] = info.earnedDescription;
                            icons[i].sprite = info.icon;
                        } else
                        {
                            titles[i].text = "";
                            descriptions[i] = info.criteriaDescription;
                        }

                        GSui.Instance.MoveIn(icons[i].transform, true);
                    }
                }
            });
        }
    }
}

