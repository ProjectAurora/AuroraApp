﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System;

namespace Aurora
{
    [RequireComponent(typeof(GAui))]
    public class LevelUpFanfare : MonoBehaviour
    {
        public Text levelText;
        private Action dismissAction;
        private GAui gaui;

        public void Start()
        {
            gaui = GetComponent<GAui>();
        }

        public void Show(int newLevel, Action dismissAction = null)
        {
            this.dismissAction = dismissAction;
            levelText.text = newLevel.ToString();
            gaui.MoveIn(GSui.eGUIMove.SelfAndChildren);
        }

        public void Dismiss()
        {
            Time.timeScale = 1;
            gaui.MoveOut(GSui.eGUIMove.SelfAndChildren);
        }

        void OnOpened()
        {
            Time.timeScale = 0;
        }

        void OnDismissed()
        {
            if (dismissAction != null)
            {
                // Use local, in case dismiss callback calls Show and sets dismissAction instance variable
                Action action = dismissAction;
                dismissAction = null;
                action.Invoke();
            }
        }
    }
}
