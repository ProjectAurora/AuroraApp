﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System;

namespace Aurora
{
    [RequireComponent(typeof(GAui))]
    public class AchievementFanfare : MonoBehaviour
    {
        public Text titleText;
        public Image iconImage;
        private Action dismissAction;
        private GAui gaui;

        public void Start()
        {
            gaui = GetComponent<GAui>();
        }

        public void Show(string achievementId, Action dismissAction = null)
        {
            this.dismissAction = dismissAction;
            AchievementResources.Info achievement = AchievementResources.Instance.FindAchievementInfo(achievementId);
            if (achievement == null)
            {
                Debug.LogError("Attempt to unlock unknown achievement: " + achievementId);
                return;
            }

            titleText.text = achievement.title;
            iconImage.sprite = achievement.icon;
            gaui.MoveIn(GSui.eGUIMove.SelfAndChildren);
        }

        public void Dismiss()
        {
            Time.timeScale = 1;
            gaui.MoveOut(GSui.eGUIMove.SelfAndChildren);
        }

        void OnOpened()
        {
            Time.timeScale = 0;
        }

        void OnDismissed()
        {
            if (dismissAction != null)
            {
                // Use local, in case dismiss callback calls Show and sets dismissAction instance variable
                Action action = dismissAction;
                dismissAction = null;
                action.Invoke();
            }
        }
    }
}
