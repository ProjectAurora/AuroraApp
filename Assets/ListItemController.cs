﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ListItemController : MonoBehaviour {

    public Image Icon;
    public Text Name;
    public Text Description;
    public Action OnClicked;

    public void OnItemClicked ()
    {
        if (OnClicked != null)
        {
            OnClicked();
        }
    }


}
