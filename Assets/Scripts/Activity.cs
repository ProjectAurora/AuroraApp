﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activity
{

    public Sprite Icon;
    public string Name;
    public string Description;
    public int PointValue;

    public Activity(Sprite icon, string name, string description, int pointValue)
    {
        Icon = icon;
        Name = name;
        Description = description;
        PointValue = pointValue;
    }
}