﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Aurora
{

    public class Player
    {
        public string objectId;
        // This refers to the ParseUser object
        public string parseUserId;
        public string name;
        public int totalPoints;
        public int totalActivitiesStarted;
        public int totalActivitiesCompleted;
        public int currentLevel;
        public int test;
        public IList<String> unlockedAchievementIds = new List<string>();

        public bool IsAchievementUnlocked(string achievementID)
        {
            return unlockedAchievementIds.Contains(achievementID);
        }

        public IList<ActivityEntry> activityEntries = new List<ActivityEntry>();
    }

}
