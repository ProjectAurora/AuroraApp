﻿//Version 1.65 (10.08.2016)

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

using UnityEngine;
using System.Collections;
using System.Threading.Tasks;

namespace Aurora
{

    public class SignOutAction : MonoBehaviour
    {
        public string nextSceneName;
        public bool exitApplication;

        public void SignOut()
        {
            Debug.Log("SignOut");
            Task<bool> isLoggedOut = CloudServices.Instance.LogOutAsync();
            isLoggedOut.ContinueWith(t =>
            {

                if ((!t.IsFaulted) && (t.Result))
                {
                    if (!exitApplication && (!string.IsNullOrEmpty(nextSceneName)))
                    {
                        LoadLevel(nextSceneName);
                    }
                }

                if (exitApplication)
                {
                    Quit();
                }

            });
        }

        private void Quit()
        {
            // Application.Quit() does not work when running in the editor
            // This is a standard way of handling this
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        }

    void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (Input.GetKeyUp(KeyCode.O) && (Input.GetKey(KeyCode.LeftAlt)))
            {
                SignOut();
            }
        }

        void LoadLevel(string levelName)
        {

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
            SceneManager.LoadScene(levelName);
#else
			Application.LoadLevel(levelName);
#endif
        }

    }
}