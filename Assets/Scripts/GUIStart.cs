﻿
using UnityEngine;
using System.Collections;


public class GUIStart : MonoBehaviour
{
	// Canvas
	public Canvas m_Canvas;

	// GAui objects of title text
	public GAui[] controls;
	
	void Awake ()
	{
		if(enabled)
		{
			// Set GSui.Instance.m_AutoAnimation to false in Awake(), let you control all GUI Animator elements in the scene via scripts.
			GSui.Instance.m_AutoAnimation = false;
		}
	}

	// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html
	void Start ()
	{

		// MoveIn m_Title1 and m_Title2
		StartCoroutine(MoveInControls());

		// Disable all scene switch buttons
		// http://docs.unity3d.com/Manual/script-GraphicRaycaster.html
		GSui.Instance.SetGraphicRaycasterEnable(m_Canvas, false);

	}
	
	// Update is called every frame, if the MonoBehaviour is enabled.
	// http://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html
	void Update ()
	{
	}
	
	
	// MoveIn m_Title1 and m_Title2
	IEnumerator MoveInControls()
	{
		yield return new WaitForSeconds(1.0f);

        foreach(GAui control in controls)
        {
            control.MoveIn(GSui.eGUIMove.SelfAndChildren);
        }

        // Enable all scene switch buttons
        StartCoroutine(EnableAllButtons());
    }
	

	// MoveOut all primary buttons
	public void HideAllGUIs()
	{
        foreach (GAui control in controls)
        {
            control.MoveOut(GSui.eGUIMove.SelfAndChildren);
        }
	}
	

	// Enable/Disable all scene switch Coroutine
	IEnumerator EnableAllButtons()
	{
		yield return new WaitForSeconds(1.0f);

		// Enable all scene switch buttons
		// http://docs.unity3d.com/Manual/script-GraphicRaycaster.html
		GSui.Instance.SetGraphicRaycasterEnable(m_Canvas, true);
	}
	
	// Disable all buttons for a few seconds
	IEnumerator DisableButtonForSeconds(GameObject GO, float DisableTime)
	{
		// Disable all buttons
		GSui.Instance.EnableButton(GO.transform, false);
		
		yield return new WaitForSeconds(DisableTime);
		
		// Enable all buttons
		GSui.Instance.EnableButton(GO.transform, true);
	}
	
	
}