﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneContentLauncher : MonoBehaviour {

    public string defaultSceneName;

    public void Launch()
    {
        string key = GlobalData.Instance.contentType + "-scene-name";
        string sceneName = GlobalData.Instance.appConfiguration.GetValue(key, defaultSceneName);
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
    }
}
