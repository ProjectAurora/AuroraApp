﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Aurora
{
    public class CreateAccount : MonoBehaviour
    {

        public InputField username;
        public InputField password;
        public InputField passwordConfirmation;
        public Text errorMessage;
        public string menuScreen;
        public BusyIndicator busyIndicator;

        void Start()
        {
            HideErrorMessage();
        }

        private void ShowErrorMessage(string msg)
        {
            errorMessage.enabled = true;
            errorMessage.text = msg;
        }

        private void HideErrorMessage()
        {
            errorMessage.enabled = false;
            errorMessage.text = "";
        }


        public void Create()
        {
            HideErrorMessage();

            if (!Validator.IsValidEmail(username.text))
            {
                ShowErrorMessage("Username must be an email address");
                return;
            }

            if (string.Compare(password.text, passwordConfirmation.text) != 0)
            {
                ShowErrorMessage("Passwords do not match");
                return;
            }

            busyIndicator.Message = "Creating account...";
            busyIndicator.IsBusy = true;
            CloudServices.Instance.CreateAccountAsync(username.text, password.text).ContinueWith(t =>
            {
                busyIndicator.IsBusy = false;

                if ((!t.IsFaulted) && (t.Result))
                {
                    busyIndicator.Message = "Signing in...";
                    busyIndicator.IsBusy = true;
                    GSui.Instance.LoadLevel(menuScreen, 0);
                } else
                {
                    ShowErrorMessage(CloudServices.Instance.LastErrorMessage);
                }
            });
        }
    }
}
