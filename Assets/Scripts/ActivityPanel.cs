﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aurora
{
    public class ActivityPanel : MonoBehaviour
    {

        public Activity activity;
        public Text title;
        public Text activityDescription;

        // Use this for initialization
        void Start()
        {
            if (activity != null)
            {
                title.text = activity.Name;
                activityDescription.text = activity.Description;
            }
        }

        void OnEnable()
        {
            if (activity != null)
            {
                title.text = activity.Name;
                activityDescription.text = activity.Description;
            }
        }

        public void SelfReportUpdate()
        {
            // CloudServices.Instance.UpdateCurrentPlayerScore(activity.PointValue, OnUpdateCompleted);
        }
        // TODO add description here, name, back button

        void OnUpdateCompleted(bool success, string errMsg)
        {
            if (success)
            {
                Debug.Log("Updated player score");
            }
            else
            {
                Debug.LogError("Couldn't update player score: " + errMsg);
            }
        }

    }
}