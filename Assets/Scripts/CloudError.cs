﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloudError : MonoBehaviour {

    public Text messageLabel;
    private Action retryCallback;
    public void Start()
    {
        if(messageLabel == null)
        {
            Debug.LogError("Message Label not assigned");
        }
    }

    // C# property
    public bool IsVisible
    {
        get
        {
            return this.gameObject.activeSelf;
        }
        set
        {
            if (value)
            {
                this.gameObject.SetActive(true);
                this.GetComponent<GAui>().MoveIn(GSui.eGUIMove.SelfAndChildren);
            } else
            {
                this.GetComponent<GAui>().MoveOut(GSui.eGUIMove.SelfAndChildren);
            }
        }
    }

    public string Message
    {
        get
        {
            return messageLabel.text;
        }
        set
        {
            messageLabel.text = value;
        }
    }

    public void Show(Action retryCallback, string details)
    {
        this.retryCallback = retryCallback;
        messageLabel.text = details;
        IsVisible = true;
    }

    public void Retry()
    {
        IsVisible = false;
    }

    public void OnClosed()
    {
        if (this.retryCallback != null)
        {
            this.retryCallback();
        }
    }

}
