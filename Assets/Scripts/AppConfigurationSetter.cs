﻿

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Aurora
{
    public class AppConfigurationSetter : MonoBehaviour
    {
        public Text text;
        public string key;
        public string defaultValue;

        public void Start()
        {
            text.text = GlobalData.Instance.appConfiguration.GetValue(key, defaultValue);
        }
    }
}