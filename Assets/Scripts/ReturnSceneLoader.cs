﻿//Version 1.65 (10.08.2016)

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif

using UnityEngine;
using System.Collections;

namespace Aurora
{
    public class ReturnSceneLoader : MonoBehaviour
    {
        public void LoadLevel()
        {
            string sceneName = GlobalData.Instance.backSceneStack.Pop();
#if UNITY_5_3 || UNITY_5_3_OR_NEWER
            SceneManager.LoadScene(sceneName);
#else
			Application.LoadLevel(sceneName);
#endif
        }

    }
}