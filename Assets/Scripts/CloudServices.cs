﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Parse;
using System;
using System.Linq;
using System.Threading.Tasks;
using TriviaQuizGame.Types;

namespace Aurora
{

    public class CloudServices : MonoBehaviour
    {

        //IEnumerable<ParseObject> activities;
        public delegate void QueryHighScoreHanldler(IEnumerable<Player> players, string errMsg);
        private List<Action> dispatcher = new List<Action>();
        public string LastErrorMessage { get; private set; }

        // this is one of the ways you can do a singleton in Unity
        private static CloudServices instance;
        public static CloudServices Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<CloudServices>();
                    if (instance == null)
                    {
                        throw new System.Exception("CloudServices does not exist in scene");
                    }
                }
                return instance;
            }
        }


        public bool isLoggedIn
        {
            get
            {
                return ParseUser.CurrentUser != null;
            }
        }

        public Task<List<Player>> QueryHighScores()
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("GameScore");

            // async task
            TaskCompletionSource<List<Player>> tcs = new TaskCompletionSource<List<Player>>();
            query.FindAsync().ContinueWith(t =>
           {
               Dispatch(() =>
               {
                   if (t.IsFaulted)
                   {
                       LogParseException(t.Exception);
                       tcs.TrySetException(t.Exception);
                   }
                   else
                   {
                       List<Player> players = new List<Player>();
                       foreach (var player in t.Result)
                       {
                           //players.Add(new Player(null, player.Get<string>("playerName"), player.Get<int>("score")));
                       }

                       tcs.TrySetResult(players);
                   }
               });
           });
            return tcs.Task;
        }

        public Task<AppConfiguration> QueryAppConfiguration()
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("AppConfiguration");

            // async task
            TaskCompletionSource<AppConfiguration> tcs = new TaskCompletionSource<AppConfiguration>();
            query.FindAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        AppConfiguration configuration = new AppConfiguration();
                        foreach (var row in t.Result)
                        {
                            configuration.settingMap[row.Get<string>("Key")] = row.Get<string>("Value");
                        }

                        tcs.TrySetResult(configuration);
                    }
                });
            });
            return tcs.Task;
        }

        public Task<List<Question>> QueryQuizQuestions(String contentType)
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("QuizQuestion").WhereEqualTo("contentType", contentType).OrderBy("questionNumber");

            // async task
            TaskCompletionSource<List<Question>> tcs = new TaskCompletionSource<List<Question>>();
            query.FindAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        List<Question> questions = new List<Question>();
                        foreach (var question in t.Result)
                        {
                            Question quizQ = new Question()
                            {
                                question = question.Get<String>("question"),
                                bonus = question.Get<int>("bonus"),
                                time = question.Get<int>("time")
                            };

                            List<Answer> downloadedAnswers = new List<Answer>();
                            for (int i=1; i<=4; ++i)
                            {
                                String key = "answer" + i.ToString();
                                if((question.ContainsKey(key)) && (!string.IsNullOrEmpty(question.Get<String>(key)))) {
                                    downloadedAnswers.Add(new Answer()
                                    {
                                        answer = question.Get<String>(key),
                                        isCorrect = question.Get<Boolean>("isCorrect" + i.ToString())
                                    });
                                }
                            }

                            quizQ.answers = downloadedAnswers.ToArray();
                            questions.Add(quizQ);
                        }

                        tcs.TrySetResult(questions);
                    }
                });
            });
            return tcs.Task;
        }

        public Task<List<Definition>> QueryBreakfreeQuestions(String contentType)
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("BreakfreeQuestion").WhereEqualTo("contentType", contentType).OrderBy("questionNumber");

            // async task
            TaskCompletionSource<List<Definition>> tcs = new TaskCompletionSource<List<Definition>>();
            query.FindAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        List<Definition> definitions = new List<Definition>();
                        foreach (var definition in t.Result)
                        {
                            Definition quizD = new Definition()
                            {
                                term = definition.Get<String>("term"),
                                definition = definition.Get<String>("definition")
                            };

                             definitions.Add(quizD);
                        }

                        tcs.TrySetResult(definitions);
                    }
                });
            });
            return tcs.Task;
        }


        public Task<Player> CreatePlayer()
        {
			Debug.Log("CloudServices.CreatePlayer()");
            TaskCompletionSource<Player> tcs = new TaskCompletionSource<Player>();
            QueryCurrentPlayer().ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    LogParseException(t.Exception);
                    tcs.TrySetException(t.Exception);
                }
                else
                {
                    if (t.Result != null)
                    {
						Debug.LogError("CreatePlayer - player already exists: " + t.Result.name);
                        Exception e = new Exception("Player already exists for " + t.Result.name);
                        LogParseException(e);
                        tcs.TrySetException(e);
                    }
                    else
                    {
						Debug.Log("Create new Player object for: " + ParseUser.CurrentUser.Username);
                        Player player = new Player()
                        {
                            parseUserId = ParseUser.CurrentUser.ObjectId,
                            name = ParseUser.CurrentUser.Username,
                            currentLevel = 1
                        };
                        UpdatePlayer(player).ContinueWith(u =>
                        {
                            if (u.IsFaulted)
                            {
                                LogParseException(u.Exception);
                                tcs.TrySetException(u.Exception);
                            } else
                            {
                                tcs.TrySetResult(u.Result);
                            }
                        });
                    }
                }
            });
            return tcs.Task;
        }

        private Player UpdateCurrentPlayerTotalPoints(Player player, int scoreDelta)
        {
			Debug.Log("CloudServices.UpdateCurrentPlayerTotalPoints() : " + scoreDelta);

            player.totalPoints += scoreDelta;

            return player;
        }

        public void SaveScore(int deltaPoints)
        {
            QueryCurrentPlayer()
                .ContinueWith(t => UpdateCurrentPlayerTotalPoints(t.Result, deltaPoints))
                .ContinueWith(t => UpdatePlayer(t.Result));
        }

        public Task<Player> QueryCurrentPlayer()
        {
            return QueryPlayer()
                .ContinueWith(t => QueryActivityEntries(t.Result))
                .Unwrap();
        }

        private Task<Player> QueryPlayer()
        {
			Debug.Log("CloudServices.QueryCurrentPlayer()");

            // This is here for development purposes only so when you run this scene in Unity, you don't get an NPE
            String currentUserID = ParseUser.CurrentUser != null ? ParseUser.CurrentUser.ObjectId : "invalidID";

            ParseQuery<ParseObject> query = ParseObject.GetQuery("Player").WhereEqualTo("parseUserId", currentUserID);

            TaskCompletionSource<Player> tcs = new TaskCompletionSource<Player>();
            query.FindAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        Player player = null;
                        var jsonPlayer = t.Result.FirstOrDefault();
                        if (jsonPlayer != null)
                        {
                            player = new Player()
                            {
                                objectId = jsonPlayer.ObjectId,
                                parseUserId = jsonPlayer.Get<string>("parseUserId"),
                                name = jsonPlayer.Get<string>("name"),
                                totalPoints = jsonPlayer.Get<int>("totalPoints"),
                                totalActivitiesStarted = jsonPlayer.Get<int>("totalActivitiesStarted"),
                                totalActivitiesCompleted = jsonPlayer.Get<int>("totalActivitiesCompleted"),
                                currentLevel = jsonPlayer.Get<int>("currentLevel"),
                                //unlockedAchievementIds = jsonPlayer.Get<IList<String>>("unlockedAchievementIds")
                            };

                            // Attempt to code around missing symbols in iOS build, may not be necessary
                            // after adding link.xml, should try switching back to 
                            foreach (string unlockedAchievementId in jsonPlayer.Get<IList<String>>("unlockedAchievementIds"))
                            {
                                player.unlockedAchievementIds.Add(unlockedAchievementId);
                            }
                        } 

                        tcs.TrySetResult(player);
                    }
                });
            });
            return tcs.Task;
        }

        public Task<Player> UpdatePlayer(Player player)
        {
			Debug.Log("CloudServices.UpdatePlayer() : " + (player.name != null ? player.name : "null"));

            ParseObject jsonPlayer = new ParseObject("Player");
            jsonPlayer.ObjectId = player.objectId;
            jsonPlayer["parseUserId"] = player.parseUserId;
            jsonPlayer["name"] = player.name;
            jsonPlayer["totalPoints"] = player.totalPoints;
            jsonPlayer["totalActivitiesStarted"] = player.totalActivitiesStarted;
            jsonPlayer["totalActivitiesCompleted"] = player.totalActivitiesCompleted;
            jsonPlayer["currentLevel"] = player.currentLevel;
			jsonPlayer.AddRangeUniqueToList<string>("unlockedAchievementIds", player.unlockedAchievementIds);

			Debug.Log ("UpdatePlayer() -> SaveAsync");
            TaskCompletionSource<Player> tcs = new TaskCompletionSource<Player>();
            jsonPlayer.SaveAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        tcs.TrySetResult(player);
                    }
                });
            });
            return tcs.Task;
        }

        public Task<bool> AddActivityEntry(string action, string detail)
        {
            Debug.LogFormat("CloudServices.AddActivityEntry({0}, {1})", action, detail);

            ParseObject parseEntry = new ParseObject("ActivityEntry");
            parseEntry["action"] = action;
            parseEntry["detail"] = detail;
            parseEntry["player"] = ParseUser.CurrentUser;

            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            parseEntry.SaveAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        tcs.TrySetResult(true);
                    }
                });
            });
            return tcs.Task;
        }

        private Task<Player> QueryActivityEntries(Player player)
        {
            Debug.Log("CloudServices.QueryActivityEntries()");

            ParseQuery<ParseObject> query = ParseObject.GetQuery("ActivityEntry").WhereEqualTo("player", ParseUser.CurrentUser);
            TaskCompletionSource<Player> tcs = new TaskCompletionSource<Player>();
            query.FindAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        if (player != null)
                        {
                            foreach (var entry in t.Result)
                            {
                                ActivityEntry newEntry = new ActivityEntry()
                                {
                                    createdAt = entry.CreatedAt,
                                    action = entry.Get<String>("action"),
                                    detail = entry.Get<String>("detail")
                                };

                                player.activityEntries.Add(newEntry);
                            }
                        }
                        tcs.TrySetResult(player);
                    }
                });
            });
            return tcs.Task;
        }

        public Task<List<Activity>> QueryActivities()
        {
            ParseQuery<ParseObject> query = ParseObject.GetQuery("Activity");

            // async task
            TaskCompletionSource<List<Activity>> tcs = new TaskCompletionSource<List<Activity>>();
            query.FindAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetException(t.Exception);
                    }
                    else
                    {
                        List<Activity> activities = new List<Activity>();
                        foreach (var activity in t.Result)
                        {
                            String iconURL = activity.Get<string>("icon");
                            Sprite iconSprite = Resources.Load(System.IO.Path.GetFileName(iconURL), typeof(Sprite)) as Sprite;
                            if (iconSprite == null)
                            {
                                iconSprite = Resources.Load("DefaultActivityIcon", typeof(Sprite)) as Sprite;
                            }
                            activities.Add(new Activity(iconSprite, activity.Get<string>("name"), activity.Get<string>("description"), activity.Get<int>("pointValue")));
                        }

                        tcs.TrySetResult(activities);
                    }
                });
            });
            return tcs.Task;
        }

        public Task<bool> LogInAsync(string username, string password)
        {
			Debug.Log("CloudServices.LogInAsync() : " + username);

            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            ParseUser.LogInAsync(username, password).ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                    //tcs.TrySetException(t.Exception);
                    tcs.TrySetResult(false);
                    }
                    else
                    {
                        tcs.TrySetResult(true);
                    }
                });

            });
            return tcs.Task;
        }

        public Task<bool> LogOutAsync()
        {
			Debug.Log("CloudServices.LogOutAsync()");

            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            ParseUser.LogOutAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        //tcs.TrySetException(t.Exception);
                        tcs.TrySetResult(false);
                    }
                    else
                    {
                        tcs.TrySetResult(true);
                    }
                });

            });
            return tcs.Task;
        }

        public Task<bool> CreateAccountAsync(string username, string password)
        {
			Debug.Log("CloudServices.CreateAccountAsync() : " + username);

            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            ParseUser user = new ParseUser();
            user.Username = username;
            user.Password = password;

            user.SignUpAsync().ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        //tcs.TrySetException(t.Exception);
                        tcs.TrySetResult(false);
                    }
                    else
                    {
                        CreatePlayer().ContinueWith(p =>
                        {
                            if (p.IsFaulted)
                            {
                                LogParseException(p.Exception);
                                tcs.TrySetException(p.Exception);
                            }
                            else
                            {
                                tcs.TrySetResult(true);
                            }
                        });
                    }
                });

            });
            return tcs.Task;
        }

        public Task<bool> PasswordResetAsync(string email)
        {
            Debug.Log("CloudServices.PasswordResetAsync() : " + email);

            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();

            ParseUser.RequestPasswordResetAsync(email).ContinueWith(t =>
            {
                Dispatch(() =>
                {
                    if (t.IsFaulted)
                    {
                        LogParseException(t.Exception);
                        tcs.TrySetResult(false);
                    }
                    else
                    {
                        tcs.TrySetResult(true);
                    }
                });

            });
            return tcs.Task;
        }

        void LogParseException(Exception e)
        {
			try
			{
	            if (e is AggregateException)
	            {
	                foreach (var innerE in ((AggregateException)e).Flatten().InnerExceptions)
	                {
	                    LastErrorMessage = innerE.Message;
	                    Debug.LogError(LastErrorMessage);
	                }
	            }
	            else
	            {
	                for (Exception currentE = e; currentE != null; currentE = currentE.InnerException)
	                {
	                    LastErrorMessage = currentE.Message;
	                    Debug.LogError(LastErrorMessage);
	                }
	            }
			}
			catch (Exception ex) 
			{
				Debug.LogError("CloudServices.LogParseException() - unable to process exception");
			}
        }

        // Update is called once per frame
        void Update()
        {
            lock (dispatcher)
            {
                while (dispatcher.Count > 0)
                {
                    try
                    {
                        dispatcher[0].Invoke();
                    }
                    catch (Exception e)
                    {
                        Debug.LogError(e);
                    }
                    finally
                    {
                        dispatcher.RemoveAt(0);
                    }                                  
                }
            }
        }

        private void Dispatch(Action a)
        {
            lock (dispatcher)
            {
                dispatcher.Add(a);
            }
        }

    }
}