﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

namespace Aurora
{
    public class DiscoverActivityController : MonoBehaviour
    {

        public Sprite[] ActivityImages;
        public GameObject ContentPanel;
        public GameObject ListItemPrefab;
        public GameObject discoverActivitiesPanel;
        public ActivityPanel activityPanel;


        private void OnEnable()
        {
            Refresh();
        }

        void Refresh()
        {
            ClearList();
            // Get the list of all the available activites
            Task<List<Activity>> activitiesTask = null;
            activitiesTask = CloudServices.Instance.QueryActivities();
            activitiesTask.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    List<Activity> activities = t.Result;
                    foreach (Activity activity in activities)
                    {
                    // newActivity is the view
                    GameObject newActivity = Instantiate(ListItemPrefab) as GameObject;
                        ListItemController controller = newActivity.GetComponent<ListItemController>();
                        if (activity.Icon != null)
                        {
                            controller.Icon.sprite = activity.Icon;
                        }
                        controller.Name.text = activity.Name;
                        controller.Description.text = activity.Description;
                        controller.OnClicked = () => ShowDetails(activity);
                        newActivity.transform.parent = ContentPanel.transform;
                        newActivity.transform.localScale = Vector3.one;
                    }
                }
            });


        }


        /*
        void Refresh()
        {
            ClearList();
            // Get the list of all the high scores
            Task<List<Activity>> activitiesTask = CloudServices.Instance.QueryActivities();
            activitiesTask.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    List<Activity> activities = t.Result;
                    foreach (Activity activity in activities)
                    {
                        GameObject newActivity = Instantiate(ListItemPrefab) as GameObject;
                        ListItemController controller = newActivity.GetComponent<ListItemController>();
                        if (activity.Icon != null)
                        {
                            controller.Icon.sprite = activity.Icon;
                        }
                        controller.Name.text = activity.TotalPoints.ToString();
                        newActivity.transform.parent = ContentPanel.transform;
                        newActivity.transform.localScale = Vector3.one;
                    }
                }
            });
        }
        */

        void ClearList()
        {
            ContentPanel.transform.DetachChildren();
        }

        /*
        void Start()
        {
            /*
            // 1. Get the data to be displayed
            Activities = new ArrayList(){
                new Activity(ActivityImages[0],
                    "Attend dbPride Meeting - 100 points",
                    "Monthly Member meeting\n1/25/17\t3:00-4:00p\nCary Southport Conference Room",
                    100),
                new Activity(ActivityImages[1],
                    "Purple Shirt Day- 50 points",
                    "Wear your purple shirt!\n1/25/17\t3:00p\nCary Lobby for Photo",
                    50),
                new Activity(ActivityImages[2],
                    "Attend Moment of Silence Ceremony - 75 points",
                    "Monthly Member meeting\n2/25/17\t3:00-3:15p\nCary Lobby",
                    75),
                new Activity(ActivityImages[3],
                    "Share your Ally story publicly - 500 points",
                    "Share your 'coming out as an Ally story'",
                    500),
                new Activity(ActivityImages[4],
                    "Display your Ally Card - 200 points",
                    "Display your ally card\nvisibly at your workspace",
                    200),
                new Activity(ActivityImages[5],
                    "Complete Ally 101 Training - 1000 points",
                    "Take this online training\nestimated time to completion: 1 hour",
                    1000)

            };
            */
        /*
        ClearList();
        // Get the list of all the high scores
        Task<List<Activity>> activitiesTask = CloudServices.Instance.QueryActivities();
        activitiesTask.ContinueWith(t =>
        {
            if (!t.IsFaulted)
            {
                List<Activity> activities = t.Result;
                foreach (Activity activity in activities)
                {
                    GameObject newActivity = Instantiate(ListItemPrefab) as GameObject;
                    ListItemController controller = newActivity.GetComponent<ListItemController>();
                    if (activity.Icon != null)
                    {
                        controller.Icon.sprite = activity.Icon;
                    }
                    controller.Name.text = activity.Name;
                    controller.Description.text = activity.Description;
                    controller.OnClicked = () => ShowDetails(activity);
                    newActivity.transform.parent = ContentPanel.transform;
                    newActivity.transform.localScale = Vector3.one;
                }
            }
        });
    
        /*

        //2.iterate through the data, 
        //      instantiate prefab,
        //      set the data,
        //      add it to panel
        foreach (Activity activity in Activities)
        {
            GameObject newActivity = Instantiate(ListItemPrefab) as GameObject;
            ListItemController controller = newActivity.GetComponent<ListItemController>();
            controller.Icon.sprite = activity.Icon;
            controller.Name.text = activity.Name;
            controller.Description.text = activity.Description;
            controller.OnClicked = () => ShowDetails(activity);
            newActivity.transform.parent = ContentPanel.transform;
            newActivity.transform.localScale = Vector3.one;
        }
        */
        //  }


        public void ShowDetails(Activity activity)
        {
            UnityEngine.Debug.Log("Activity activated " + activity.Name);
            // add enum for the Activity types
            // add a list/dictionary that maps Activity types to a view game object
            // show the view

            //SelfReportingActivityPanel.
            activityPanel.activity = activity;
            activityPanel.gameObject.SetActive(true);

            discoverActivitiesPanel.SetActive(false);

        }
    }
}