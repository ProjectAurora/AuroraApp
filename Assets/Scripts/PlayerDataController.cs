﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.Events;

namespace Aurora {
    public class PlayerDataController : MonoBehaviour {

        public Text totalScore;
        public AchievementFanfare achievementFanfare;
        public LevelUpFanfare levelFanfare;
        public CloudError cloudError;
        private Queue<Action> fanfareQueue = new Queue<Action>();
        [Serializable]
        public class LevelEnabled
        {
            public int minimumLevel;
            public Selectable selectable; 
        }
        public LevelEnabled[] levelEnabled;
        public UnityEvent onPlayerLoaded;


        // Use this for initialization
        void Start() {
            if (cloudError == null)
            {
                Debug.LogError("CloudError: Field has not been set");
            }
            LoadPlayerData();
        }

        private void LoadPlayerData()
        {
            Task<Player> queryPlayer = CloudServices.Instance.QueryCurrentPlayer();
            queryPlayer.ContinueWith(t =>
            {
                if ((!t.IsFaulted) && (t.Result!=null))
                {
                    EnableControlsByLevel(t.Result);
                    if (totalScore != null)
                    {
                        totalScore.text = t.Result.totalPoints.ToString();
                        GAui control = totalScore.GetComponent<GAui>();
                        control.MoveIn(GSui.eGUIMove.SelfAndChildren);
                    }

                    onPlayerLoaded.Invoke();

                } else
                {
                    cloudError.Show(LoadPlayerData, "Can't download player information");
                }
            });
        }

        private void EnableControlsByLevel(Player player)
        {
            foreach (LevelEnabled level in levelEnabled)
            {
                level.selectable.interactable = player.currentLevel >= level.minimumLevel;

            }
        }

        private delegate Player UpdatePlayerHandler(Player player);

        private void UpdatePlayerData(UpdatePlayerHandler updatePlayer)
        {
            CloudServices.Instance.QueryCurrentPlayer()
            .ContinueWith(t => updatePlayer(t.Result))
            .ContinueWith(t => CloudServices.Instance.UpdatePlayer(t.Result))
            .ContinueWith(t => ProcessFanfareQueue());
        }

        public void UpdateStats(int score, string activityName = null, bool isComplete = false)
        {
            if (String.IsNullOrEmpty(activityName))
            {
                UpdatePlayerData((player) =>
                {
                    player.totalPoints += score;
                    return player;
                });
            }
            else
            {
                string action = (isComplete ? "completed" : "started");
                CloudServices.Instance.AddActivityEntry(action, activityName)
                    .ContinueWith(t =>
                    {
                        UpdatePlayerData((player) =>
                        {
                            player.totalPoints += score;
                            if (isComplete)
                            {
                                player.totalActivitiesCompleted++;
                            }
                            else
                            {
                                player.totalActivitiesStarted++;
                            }

                            CheckForAchievementUnlocks(player);
                            CheckForLevelUps(player);

                            return player;
                        });
                    });
            }

        }

        private void CheckForAchievementUnlocks(Player player)
        {
            int uniqueActivitiesCompleted = NumberOfUniqueCompletedActivites(player);
            double daysBetweenActivities = NumberOfDaysBetweenActivities(player);
            int uniquesActivitiesInADay = NumberOfUniqueActivitesInADay(player);

            if ((!player.IsAchievementUnlocked("FirstActivity")) && (player.totalActivitiesStarted > 0))
            {
                UnlockAchievement(player, "FirstActivity");
            }
            if ((!player.IsAchievementUnlocked("CompletedTwoActivities")) && (player.totalActivitiesCompleted > 1))
            {
                UnlockAchievement(player, "CompletedTwoActivities");
            }
            if ((!player.IsAchievementUnlocked("Nibbler")) && (uniquesActivitiesInADay > 4))
            {
                UnlockAchievement(player, "Nibbler");
            }
            if ((!player.IsAchievementUnlocked("CameBackForMore")) && (daysBetweenActivities > 1))
            {
                UnlockAchievement(player, "CameBackForMore");
            }
            if ((!player.IsAchievementUnlocked("CompletedEverything")) && (uniqueActivitiesCompleted > 5))
            {
                UnlockAchievement(player, "CompletedEverything");
            }

        }

        static int NumberOfUniqueCompletedActivites(Player player)
        {
            if (player.activityEntries.Count == 0)
            {
                return 0;
            }

            return player.activityEntries.Where(e => e.action == "completed").Select(e => e.detail).Distinct().Count();
        }

        // Nibbler
        static int NumberOfUniqueActivitesInADay(Player player)
        {
            if (player.activityEntries.Count == 0)
            {
                return 0;
            }

            return player.activityEntries.Where(e => e.action == "started")
                .GroupBy(e => e.createdAt.Value.ToString("d")).Select(g => g.Count()).OrderByDescending(g => g).First();
        }

        static double NumberOfDaysBetweenActivities(Player player)
        {
            if (player.activityEntries.Count == 0)
            {
                return 0;
            }

            ActivityEntry oldest = player.activityEntries.Where(e => e.action == "started").OrderBy(x => x.createdAt).First();
            ActivityEntry mostRecent = player.activityEntries.Where(e => e.action == "started").OrderByDescending(x => x.createdAt).First();

            return (mostRecent.createdAt - oldest.createdAt).Value.TotalDays;
        }

        public Player UnlockAchievement(Player player, string achievementId)
        {
            Debug.Log("CloudServices.UnlockCurrentPlayerAchievement() : " + achievementId);

            if (!player.IsAchievementUnlocked(achievementId))
            {
                player.unlockedAchievementIds.Add(achievementId);
                fanfareQueue.Enqueue(() =>
                {
                    achievementFanfare.Show(achievementId, ProcessFanfareQueue);
                });
            }
            else
            {
                Debug.LogWarning("Achievement already unlocked: " + achievementId);
            }

            return player;
        }

        private void CheckForLevelUps(Player player)
        {
            int level1ActivitiesCompleted = player.activityEntries.Where(e => e.action == "completed" && e.detail.Contains("-1")).Select(e => e.detail).Distinct().Count();
            if (level1ActivitiesCompleted >= 3 && player.currentLevel < 2)
            {
                player.currentLevel++;
                fanfareQueue.Enqueue(() =>
                {
                    levelFanfare.Show(player.currentLevel, ProcessFanfareQueue);
                });
            }
        }

        private void ProcessFanfareQueue()
        {
            if(fanfareQueue.Count > 0)
            {
                fanfareQueue.Dequeue().Invoke();
            }
        }
    }
}