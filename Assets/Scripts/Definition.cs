﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aurora
{
    [Serializable]
    public class Definition
    {
        public string term;
        public string definition;
    }
}
