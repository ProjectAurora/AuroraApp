﻿using Aurora;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TriviaQuizGame;
using TriviaQuizGame.Types;
using UnityEngine;

public class BreakfreeDataLoader : MonoBehaviour {

    public string devContentType;
    public BreakfreeGameManager breakfreeGame;
    public BusyIndicator busyIndicator;
    public CloudError cloudError;
	
    public void Load()
    {
        busyIndicator.Message = "Loading...";
        busyIndicator.IsBusy = true;

        string content = GlobalData.Instance.contentType;
        if (string.IsNullOrEmpty(content) && Application.isEditor)
        {
            content = devContentType;
        }

        Task<List<Definition>> queryQuizQuestion = CloudServices.Instance.QueryBreakfreeQuestions(content);

        queryQuizQuestion.ContinueWith(t =>
        {
            busyIndicator.IsBusy = false;

            if ((!t.IsFaulted)  && (t.Result != null))
            {
                Debug.Log("Downloaded Quiz Info");
                breakfreeGame.definitions = t.Result.ToArray();
                breakfreeGame.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogWarning("Could not download Quiz Info");
                cloudError.Show(Load, CloudServices.Instance.LastErrorMessage);
            }
        });
    }
}
