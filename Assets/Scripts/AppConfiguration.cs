﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aurora
{
    public class AppConfiguration
    {
        public Dictionary<string, string> settingMap = new Dictionary<string, string>();

        public bool HasKey(string key)
        {
            return settingMap.ContainsKey(key);
        }

        public string GetValue(string key, string defaultValue=null)
        {
            if(HasKey(key))
            {
                return settingMap[key];
            }
            else if (defaultValue != null)
            {
                return defaultValue;
            } else
            {
                throw new Exception("App Configuration does not contain key: " + key);
            }
        }
    }
}
