﻿using Aurora;
using System.Collections.Generic;
using UnityEngine;

/// <summary>Manages data for persistance between levels.</summary>
public class GlobalData : MonoBehaviour
{
    public string contentType;
    public Stack<string> backSceneStack = new Stack<string>();
    public AppConfiguration appConfiguration;

    // this is one of the ways you can do a singleton in Unity
    private static GlobalData instance;
    public static GlobalData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GlobalData>();
                if (instance == null)
                {
                    throw new System.Exception("GlobalData does not exist in scene");
                }
            }
            return instance;
        }
    }

    /// <summary>Awake is called when the script instance is being loaded.</summary>
    void Awake()
    {
        // If the instance reference has not been set, yet, 
        if (instance == null)
        {
            // Set this instance as the instance reference.
            instance = this;
        }
        else if (instance != this)
        {
            // If the instance reference has already been set, and this is not the
            // the instance reference, destroy this game object.
            Destroy(gameObject);
        }

        // Do not destroy this object, when we load a new scene.
        DontDestroyOnLoad(gameObject);
    }
}