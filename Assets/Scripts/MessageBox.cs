﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour {

    public Text messageLabel;
    public Text title;

    private Action dismissCallback;

    // C# property
    public bool IsVisible
    {
        get
        {
            return this.gameObject.activeSelf;
        }
        set
        {
            if (value)
            {
                this.gameObject.SetActive(true);
                this.GetComponent<GAui>().MoveIn(GSui.eGUIMove.SelfAndChildren);
            } else
            {
                this.GetComponent<GAui>().MoveOut(GSui.eGUIMove.SelfAndChildren);
            }
        }
    }

    public void Show(string title, string message, Action dismissCallback = null)
    {
        this.dismissCallback = dismissCallback;
        messageLabel.text = message;
        this.title.text = title;
        IsVisible = true;
    }

    public void Close()
    {
        IsVisible = false;
    }

    public void OnClosed()
    {
        if (this.dismissCallback != null)
        {
            this.dismissCallback();
        }
    }

}
