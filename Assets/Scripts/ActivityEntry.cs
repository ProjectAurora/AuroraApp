﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Aurora
{

    public class ActivityEntry
    {
        public DateTime? createdAt;
        public string action;
        public string detail;
    }
}
