﻿using Aurora;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TriviaQuizGame;
using TriviaQuizGame.Types;
using UnityEngine;

public class QuizDataLoader : MonoBehaviour {

    public CloudError cloudError;
    public TQGGameController quizGame;
    public BusyIndicator busyIndicator;

    public void Load()
    {
        busyIndicator.Message = "Loading...";
        busyIndicator.IsBusy = true;

        string content = GlobalData.Instance.contentType;
        Task<List<Question>> queryQuizQuestion = CloudServices.Instance.QueryQuizQuestions(content);

        queryQuizQuestion.ContinueWith(t =>
        {
            busyIndicator.IsBusy = false;

            if ((!t.IsFaulted)  && (t.Result != null))
            {
                Debug.Log("Downloaded Quiz Info");
                if (t.Result.Count == 0)
                {
                    cloudError.Show(Load, "Unable to download questions");
                    return;
                }
                quizGame.questions = t.Result.ToArray();
                quizGame.gameObject.SetActive(true);
            } else
            {
                Debug.LogWarning("Could not download Quiz Info");
                cloudError.Show(Load, CloudServices.Instance.LastErrorMessage);
            }
        });
    }
}
