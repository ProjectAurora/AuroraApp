﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UrlContentLauncher : MonoBehaviour {

    public string defaultURL;

    public void Launch()
    {
        string key = GlobalData.Instance.contentType + "-url";
        string url = GlobalData.Instance.appConfiguration.GetValue(key, defaultURL);
        Application.OpenURL(url);
    }
}
