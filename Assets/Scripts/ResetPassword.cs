﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Aurora
{
    public class ResetPassword : MonoBehaviour
    {

        public InputField username;
        public Text errorMessage;
        public Transform confirmationPanel;
        public BusyIndicator busyIndicator;


        // Use this for initialization
        void Start()
        {
            HideErrorMessage();
        }

        private void ShowErrorMessage(string msg)
        {
            errorMessage.enabled = true;
            errorMessage.text = msg;
        }

        private void HideErrorMessage()
        {
            errorMessage.enabled = false;
            errorMessage.text = "";
        }


        public void Reset()
        {
            HideErrorMessage();

            if (!Validator.IsValidEmail(username.text))
            {
                ShowErrorMessage("Invalid email address");
                return;
            }

            busyIndicator.Message = "Resetting password...";
            busyIndicator.IsBusy = true;
            Task<bool> isLoggedIn = CloudServices.Instance.PasswordResetAsync(username.text);
            isLoggedIn.ContinueWith(t =>
            {
                busyIndicator.IsBusy = false;

                if ((!t.IsFaulted) && (t.Result))
                {
                    GSui.Instance.MoveOut(this.gameObject.transform, true);
                    GSui.Instance.MoveIn(confirmationPanel, true);
                } else
                {
                    ShowErrorMessage(CloudServices.Instance.LastErrorMessage);
                }
            });
        }
    }
}
