using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Aurora
{
    //Make sure there is always an AudioSource component on the GameObject where this script is added.
    [RequireComponent(typeof(AudioSource))]
    public class BreakfreeGameManager : MonoBehaviour
    {
        public bool devMode;
        public bool endRoundOnWordBlockBreak;
        public float waitTimeBetweenLevels;

        public PlayerDataController playerDataController;

        //Text element to display certain messages on
        public Text FeedbackText;
        public Text questionLabel;
        public Text answerLabel;
        public GameObject messagePanel;
        public GameObject buttonPanel;

        public GameObject blocksPrefab;
        public GameObject wordBlockPrefab;
        public GameObject ballPrefab;

        public Paddle paddle;

        //Text to be displayed when entering one of the gamestates
        public string GameNotStartedText;
        public string GameCompletedText;
        public string GameFailedText;

        public float gameSpeed;

        public Definition[] definitions;

        //Sounds to be played when entering one of the gamestates
        public AudioClip StartSound;
        public AudioClip FailedSound;

        private bool isInteractable;
        public bool IsInteractable
        {
            get{ return isInteractable; }
            set
            {
                isInteractable = value;
                paddle.isInteractable = value;
            }
        }
        private enum BreakFreeGameState
        {
            Ready,
            Playing,
            Pause,
            Resume,
            LevelComplete,
            GameOver,
            Won,
            WaitingToLaunchBall
        }

        private BreakFreeGameState currentState = BreakFreeGameState.Ready;
        //All the blocks found in this level, to keep track of how many are left
        private List<Block> allBlocks = new List<Block>();
        private List<Ball> allBalls = new List<Ball>();
        private int currentLevel;
        private Vector3 initialPaddleLocation;
        private int ballWhenPaused;

        // Use this for initialization
        void Start()
        {
            playerDataController.UpdateStats(0, GlobalData.Instance.contentType);

            Time.timeScale = 1;

            // DismissMessagePanel();

            initialPaddleLocation = paddle.transform.position;

            //Prepare the start of the level
            SwitchTo(BreakFreeGameState.Ready);

            //GSui.Instance.MoveOut(messagePanel.transform, true);
            GSui.Instance.Reset(messagePanel.transform);

        }

        public void HideAllGUIs()
        {
            GSui.Instance.MoveOut(messagePanel.transform, true);
        }

        // Update is called once per frame
        void Update()
        {
            switch (currentState)
            {
                case BreakFreeGameState.WaitingToLaunchBall:
                    CheckEndOfGame();
                    //Check if the player taps/clicks.
                    if (IsInteractable && Input.GetMouseButtonDown(0))    //Note: on mobile this will translate to the first touch/finger so perfectly multiplatform!
                    {
                        foreach (Ball ball in allBalls)
                        {
                            ball.Launch();
                        }

                        SwitchTo(BreakFreeGameState.Playing);
                    }
                    break;
                case BreakFreeGameState.Playing:
                    CheckEndOfGame();
                    break;
                case BreakFreeGameState.Pause:
                    break;
                case BreakFreeGameState.Resume:
                    CheckEndOfGame();
                    //Check if the player taps/clicks.
                    if (IsInteractable && Input.GetMouseButtonDown(0))    //Note: on mobile this will translate to the first touch/finger so perfectly multiplatform!
                    {
                        foreach (Ball ball in allBalls)
                        {
                            ball.Launch();
                        }
                        SwitchTo(BreakFreeGameState.Playing);
                    }
                    break;
                //Both cases do the same: restart the game
                case BreakFreeGameState.Won:
                    //Restart();
                    break;
            }
        }

        //Do the appropriate actions when changing the gamestate
        private void SwitchTo(BreakFreeGameState newState)
        {
            currentState = newState;
            AudioSource audio = GetComponent<AudioSource>();

            switch (currentState)
            {
                case BreakFreeGameState.Ready:
                    IsInteractable = false;
                    InitializeLevel();
                    DisplayText(String.Format("Ready!\n Level {0} of {1}", currentLevel + 1, definitions.Length));
                    StartCoroutine(InvokeAfter(waitTimeBetweenLevels, ()=>SwitchTo(BreakFreeGameState.WaitingToLaunchBall)));
                    break;
                case BreakFreeGameState.WaitingToLaunchBall:
                    IsInteractable = true;
                    DisplayText(String.Format("Tap paddle to start game"));
                    break;
                case BreakFreeGameState.Playing:
                    audio.PlayOneShot(StartSound);
                    DisplayText("");
                    break;
                case BreakFreeGameState.LevelComplete:
                    IsInteractable = false;
                    DestroyBalls();
                    DestroyPowerUps();
                    audio.PlayOneShot(StartSound);
                    int points = (currentLevel + 1) * 50;
                    currentLevel++;
                    DisplayText(String.Format("Level {0} of {1} Completed!\n+{2} points", currentLevel, definitions.Length, points));
                    StartCoroutine(InvokeAfter(waitTimeBetweenLevels, SwitchToNextLevel));
                    if (HaveWon)
                    {
                        playerDataController.UpdateStats(points, GlobalData.Instance.contentType, true);
                    }
                    else
                    {
                        playerDataController.UpdateStats(points);
                    }
                    break;
                case BreakFreeGameState.Pause:
                    IsInteractable = false;
                    ballWhenPaused = allBalls.Count;
                    DestroyBalls();
                    DestroyPowerUps();
                    break;
                case BreakFreeGameState.Resume:
                    IsInteractable = true;
                    for (int i = 0; i < ballWhenPaused; i++)
                    {
                        CreateNewBall();
                    }
                    break;
                case BreakFreeGameState.GameOver:
                    IsInteractable = false;
                    DestroyPowerUps();
                    audio.PlayOneShot(FailedSound);
                    DisplayText(String.Format("Game Over", currentLevel, definitions.Length));
                    ShowButtonPanel();
                    break;
                case BreakFreeGameState.Won:
                    IsInteractable = false;
                    DisplayText(String.Format("You Won!", currentLevel, definitions.Length));
                    ShowButtonPanel();
                    break;
            }
        }


        private void DestroyBalls()
        {
            foreach (Ball ball in allBalls)
            {
                if (ball != null)
                {
                    DestroyObject(ball.gameObject);
                }
            }
            allBalls.Clear();
        }


        private void DestroyBlocks()
        {
            foreach (Block block in FindObjectsOfType<Block>())
            {
                DestroyObject(block.gameObject);
            }
            allBlocks.Clear();
        }

        private void DestroyPowerUps()
        {
            foreach (PowerUpBase powerup in FindObjectsOfType<PowerUpBase>())
            {
                DestroyObject(powerup.gameObject);
            }
        }

        //Helper to display some text
        private void DisplayText(string text)
        {
            FeedbackText.text = text;
        }

        //Coroutine which waits and then restarts the level
        //Note: You need to call this method with StartRoutine(RestartAfter(seconds)) else it won't restart
        private IEnumerator InvokeAfter(float seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);

            action();
        }

        //Helper to restart the level
        public void Restart()
        {
            DismissButtonPanel();
            currentLevel = 0;
            SwitchTo(BreakFreeGameState.Ready);
        }

        public void Exit()
        {
            DisplayText("");
            DismissButtonPanel();
            DestroyBlocks();

            string returnScene = GlobalData.Instance.backSceneStack.Pop();
            GSui.Instance.LoadLevel(returnScene, 0f);
        }

        private void SwitchToNextLevel()
        {
            if (HaveWon)
            {
                SwitchTo(BreakFreeGameState.Won);
            }
            else
            {
                SwitchTo(BreakFreeGameState.Ready);
            }
        }

        private bool HaveWon
        {
            get { return currentLevel == definitions.Length; }
        }

        private void InitializeLevel()
        {
            if (definitions.Length == 0)
            {
                SwitchTo(BreakFreeGameState.GameOver);
                return;
            }

            // delete current blocks if any exist
            DestroyBlocks();
            Time.timeScale = gameSpeed;

            if (!devMode)
            {
                // create the blocks
                GameObject blocks = Instantiate(blocksPrefab);
                foreach (Block block in blocks.GetComponentsInChildren<Block>())
                {
                    block.OnBroken += OnBlockBroken;
                    // disable powerups
                    DropPowerUpOnHit powerUp = block.GetComponent<DropPowerUpOnHit>();
                    if (powerUp != null)
                    {
                        Destroy(powerUp);
                    }
                    allBlocks.Add(block);
                }
            }

            Block wordBlock = Instantiate(wordBlockPrefab).GetComponent<Block>();
            wordBlock.OnBroken += OnBlockBroken;
            wordBlock.OnBroken += OnWordBlockBroken;
            wordBlock.GetComponent<WordSetter>().Set(definitions[currentLevel].term);
            allBlocks.Add(wordBlock);

            CreateNewBall();

            paddle.transform.position = initialPaddleLocation;

        }

        private void CreateNewBall()
        {
            Ball ball = Instantiate(ballPrefab).GetComponent<Ball>();
            NewBallCreated(ball);
        }

        internal void NewBallCreated(Ball ball)
        {
            allBalls.Add(ball);
            ball.OnBallLost += OnBallLost;
        }

        private void OnBallLost(Ball b)
        {
            allBalls.Remove(b);
        }

        private void OnBlockBroken(Block b)
        {
            allBlocks.Remove(b);
        }

        private void OnWordBlockBroken(Block b)
        {
            RandomColor color = b.GetComponent<RandomColor>();

            messagePanel.GetComponent<Image>().color = color.backgroundColor;
            questionLabel.color = color.foregroundColor;
            answerLabel.color = color.foregroundColor;

            questionLabel.text = definitions[currentLevel].definition;
            answerLabel.text = definitions[currentLevel].term;
            if (endRoundOnWordBlockBreak)
            {
                DestroyBlocks();
            }
            GSui.Instance.MoveIn(messagePanel.transform, true);

            // switch the state to DisplayMessage
            SwitchTo(BreakFreeGameState.Pause);
        }

        public void DismissMessagePanel()
        {
            //messagePanel.SetActive(false);
            GSui.Instance.MoveOut(messagePanel.transform, true);
            SwitchTo(BreakFreeGameState.Resume);
        }

        public void DismissButtonPanel()
        {
            GSui.Instance.MoveOut(buttonPanel.transform, true);
        }

        public void ShowButtonPanel()
        {
            buttonPanel.gameObject.SetActive(true);
            GSui.Instance.MoveIn(buttonPanel.transform, true);
        }

        private void CheckEndOfGame()
        {
            //Are there no balls left?
            if (allBalls.Count == 0)
            {
                SwitchTo(BreakFreeGameState.GameOver);
            }
            if (allBlocks.Count == 0)
            {
                SwitchTo(BreakFreeGameState.LevelComplete);
            }
        }
    }
}