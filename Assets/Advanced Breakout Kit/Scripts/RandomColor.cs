using UnityEngine;

public class RandomColor : MonoBehaviour
{
    public Color backgroundColor;
    public Color foregroundColor;

    //Start is called one time when the scene has been loaded
    void Start()
    {
        //Grab the material on this gameobject and set its main color to a random value.
        backgroundColor = new Color(Random.value, Random.value, Random.value);
        foregroundColor = ContrastColor(backgroundColor);
        this.GetComponent<Renderer>().material.color = backgroundColor;
    }

    Color ContrastColor(Color color)
    {
        int d = 0;

        // Counting the perceptive luminance - human eye favors green color... 
        double a = 1 - (0.299 * color.r + 0.587 * color.g + 0.114 * color.b);

        if (a < 0.5)
            d = 0; // bright colors - black font
        else
            d = 255; // dark colors - white font

        return new Color(d, d, d);
    }
}