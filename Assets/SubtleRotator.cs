﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubtleRotator : MonoBehaviour {

    public float rangeInDegrees;
    public float speed;
    private int frame;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
       // transform.Rotate(0, Mathf.PingPong(speed * Time.deltaTime, rangeInDegrees), 0);
        transform.eulerAngles = new Vector3(0, Mathf.PingPong(speed * frame++, rangeInDegrees), 0);
    }
}
