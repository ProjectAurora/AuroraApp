﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GAui))]
public class BusyIndicator : MonoBehaviour {

    public Text messageLabel;

    // C# property
    public bool IsBusy
    {
        get
        {
            return this.gameObject.activeSelf;
        }
        set
        {
            if (value)
            {
                this.gameObject.SetActive(true);
                this.GetComponent<GAui>().MoveIn(GSui.eGUIMove.SelfAndChildren);
            } else
            {
                this.GetComponent<GAui>().MoveOut(GSui.eGUIMove.SelfAndChildren);
            }
        }
    }

    public string Message
    {
        get
        {
            return messageLabel.text;
        }
        set
        {
            messageLabel.text = value;
        }
    }	
}
