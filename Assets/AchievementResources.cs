﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aurora
{
    public class AchievementResources : MonoBehaviour
    {

        [Serializable]
        public class Info
        {
            public string id;
            public string title;
            public string earnedDescription;
            public string criteriaDescription;
            public Sprite icon;
        }
        public Info[] achievementInfo;

        // this is one of the ways you can do a singleton in Unity
        private static AchievementResources instance;
        public static AchievementResources Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<AchievementResources>();
                    if (instance == null)
                    {
                        throw new System.Exception("AchievementResources prefab does not exist in scene");
                    }
                }
                return instance;
            }
        }

        public Info FindAchievementInfo(string achievementId)
        {
            return achievementInfo.FirstOrDefault(i => i.id == achievementId);
        }
    }
}
