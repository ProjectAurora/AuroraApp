﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurora
{
    public class ComicViewer : MonoBehaviour
    {
        public Camera frameCamera;
        public Transform cameraPositions;
        public float timeBetweenFrames;
        public GETween.GETweenType frameEase;
        public string quizSceneName;

        private bool detectSwipeInput;
        private int frame;
        private GETween tweener;

        public void Start()
        {
            tweener = GetComponent<GETween>();
            MoveCameraTo(cameraPositions.GetChild(0));
            StartViewing();
        }
        public void LaunchReturnScene()
        {
            string returnScene = GlobalData.Instance.backSceneStack.Pop();
            GSui.Instance.LoadLevel(returnScene, 0f);
        }

        public void StartViewing()
        {
            Debug.Log("Comic Viewing started");
            detectSwipeInput = true;
        }

        public void NextFrame()
        {
            ++frame;
            if (frame < cameraPositions.childCount)
            {
                MoveCameraTo(cameraPositions.GetChild(frame));
            }
            else
            {
                GSui.Instance.LoadLevel(quizSceneName, 0f);
            }
        }

        public void PreviousFrame()
        {
            if (frame==0)
            {
                string returnScene = GlobalData.Instance.backSceneStack.Pop();
                GSui.Instance.LoadLevel(returnScene, 0f);
            } else
            {
                frame = Mathf.Max(frame - 1, 0);
                MoveCameraTo(cameraPositions.GetChild(frame));
            }
        }

        private void MoveCameraTo(Transform target)
        {
            Vector3 startPos = frameCamera.transform.position;
            Vector3 startLocalScale = frameCamera.transform.localScale;
            Quaternion startRotation = frameCamera.transform.rotation;
            GETween.TweenValue(this.gameObject, (value) =>
            {
                frameCamera.transform.position = Vector3.Lerp(startPos, target.position, value);
                frameCamera.transform.localScale = Vector3.Lerp(startLocalScale, target.localScale, value);
                frameCamera.transform.rotation = Quaternion.Lerp(startRotation, target.rotation, value);
            }, 0, 1, timeBetweenFrames).m_TweenType = frameEase;
        }

        private void OnSwipeLeft()
        {
            Debug.Log("OnSwipeLeft");
            NextFrame();
        }

        private void OnSwipeRight()
        {
            Debug.Log("OnSwipeRight");
            PreviousFrame();
        }

        public float maxTime;
        public float minSwipeDist;

        float startTime;
        float endTime;

        Vector3 startPos;
        Vector3 endPos;
        float swipeDistance;
        float swipeTime;

        // Update is called once per frame
        void Update()
        {
            if (!detectSwipeInput)
            {
                return;
            }

            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    startTime = Time.time;
                    startPos = touch.position;
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    endTime = Time.time;
                    endPos = touch.position;

                    swipeDistance = (endPos - startPos).magnitude;
                    swipeTime = endTime - startTime;

                    if (swipeTime < maxTime && swipeDistance > minSwipeDist)
                    {
                        SwipeFunc();
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    startTime = Time.time;
                    startPos = Input.mousePosition;
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    endTime = Time.time;
                    endPos = Input.mousePosition;

                    swipeDistance = (endPos - startPos).magnitude;
                    swipeTime = endTime - startTime;

                    if (swipeTime < maxTime && swipeDistance > minSwipeDist)
                    {
                        SwipeFunc();
                    }
                }
            }
        }

        void SwipeFunc()
        {
            Vector2 distance = endPos - startPos;
            if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
            {
                Debug.Log("Horizontal swipe");
                if (startPos.x > endPos.x)
                {
                    OnSwipeLeft();
                }
                else
                {
                    OnSwipeRight();
                }
            }
            else if (Mathf.Abs(distance.x) < Mathf.Abs(distance.y))
            {
                Debug.Log(" vertical  swipe");
            }
        }
    }
}