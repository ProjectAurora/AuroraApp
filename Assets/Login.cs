﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

namespace Aurora
{
    public class Login : MonoBehaviour
    {

        public InputField username;
        public InputField password;
        public Text errorMessage;
        public string menuScreen;
        public BusyIndicator busyIndicator;

        void Start()
        {
            HideErrorMessage();
        }

        private void ShowErrorMessage(string msg)
        {
            errorMessage.enabled = true;
            errorMessage.text = msg;
        }

        private void HideErrorMessage()
        {
            errorMessage.enabled = false;
            errorMessage.text = "";
        }

        public void LogIn()
        {
            HideErrorMessage();

            if (!Validator.IsValidEmail(username.text))
            {
                ShowErrorMessage("Username must be an email address");
                return;
            }

            busyIndicator.Message = "Signing in...";
            busyIndicator.IsBusy = true;
            Task<bool> isLoggedIn = CloudServices.Instance.LogInAsync(username.text, password.text);
            isLoggedIn.ContinueWith(t =>
            {
                busyIndicator.IsBusy = false;

                if ((!t.IsFaulted) && (t.Result))
                {
                    GSui.Instance.LoadLevel(menuScreen, 0);
                } else
                {
                    ShowErrorMessage(CloudServices.Instance.LastErrorMessage);
                }
            });
        }




    }
}
