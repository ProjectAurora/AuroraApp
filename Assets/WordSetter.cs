﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordSetter : MonoBehaviour {

    public Text wordLabel;

    public void Set(string word)
    {
        wordLabel.text = word;
    }

    public void Start()
    {
        wordLabel.color = GetComponent<RandomColor>().foregroundColor;
    }
}
