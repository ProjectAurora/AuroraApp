﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
namespace Aurora
{

    public class LeaderboardController : MonoBehaviour
    {

        public Sprite[] ActivityImages;
        public GameObject ContentPanel;
        public GameObject ListItemPrefab;

        private void OnEnable()
        {
            Refresh();
        }

        void Refresh()
        {
            ClearList();
            // Get the list of all the high scores
            Task<List<Player>> playersTask = CloudServices.Instance.QueryHighScores();
            playersTask.ContinueWith(t =>
            {
                if (!t.IsFaulted)
                {
                    List<Player> players = t.Result;
                    foreach (Player player in players)
                    {
                        GameObject newActivity = Instantiate(ListItemPrefab) as GameObject;
                        ListItemController controller = newActivity.GetComponent<ListItemController>();
                        //if (player.Icon != null)
                        //{
                        //    controller.Icon.sprite = player.Icon;
                        //}
                        //controller.Name.text = player.TotalPoints.ToString();
                        newActivity.transform.parent = ContentPanel.transform;
                        newActivity.transform.localScale = Vector3.one;
                    }
                }
            });



        }

        void ClearList()
        {
            ContentPanel.transform.DetachChildren();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}