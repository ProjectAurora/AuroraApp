﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Aurora
{

    public class SignInManager : MonoBehaviour
    {

        public GAui signInPanel;
        public GAui signUpPanel;
        public string menuScreen;
        public CloudError cloudError;

        public void Start()
        {
            LoadConfiguration();
        }

        private void LoadConfiguration()
        {
            CloudServices.Instance.QueryAppConfiguration().ContinueWith(t =>
            {
                if ((!t.IsFaulted) && (t.Result != null))
                {
                    AppConfiguration appConfiguration = t.Result;
                    Debug.Log("Downloaded App Configuration");
                    if (appConfiguration.settingMap.Count == 0)
                    {
                        cloudError.Show(LoadConfiguration, "Unable to contact server");
                        return;
                    }
                    GlobalData.Instance.appConfiguration = appConfiguration;
                    Transition();
                }
                else
                {
                    Debug.LogWarning("Could not download App Configuration");
                    cloudError.Show(LoadConfiguration, CloudServices.Instance.LastErrorMessage);
                }
            });
        }

        private void Transition()
        {
            if (CloudServices.Instance.isLoggedIn)
            {
                GSui.Instance.LoadLevel(menuScreen, 0);
            }
            else
            {
                signInPanel.gameObject.SetActive(true);
                GSui.Instance.MoveIn(signInPanel.transform, true);
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }

        public void Show(Transform transform)
        {
            GSui.Instance.MoveIn(transform, true);
        }

        public void Hide(Transform transform)
        {
            GSui.Instance.MoveOut(transform, true);
        }
    }
}