﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Aurora
{
    public class CreditsManager : MonoBehaviour
    {

        private GETween tweener;
        public ScrollRect scrollView;
        public float scrollSpeed;

        public void Start()
        {
            tweener = GetComponent<GETween>();
            ScrollCredits();
        }

        private void ScrollCredits()
        {
            GETween.TweenValue(this.gameObject, (value) =>
            {
                scrollView.verticalNormalizedPosition = value;
            }, 1, 0, scrollSpeed).m_TweenType = GETween.GETweenType.linear;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

