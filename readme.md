# Aurora
> Project Aurora is a multi-platform mobile application that uses game tech to educate and encourage allies for the LGBT+ community. It is a collection of minigames and interactive media experiences that educate and reward allies and potential allies using points and achievements. 
![](header.png)

## Architecture
![alt-text](Assets/Documentation/ArchitectureDocument.png)

## Development setup

1. Download and install the personal verion of [Unity](https://store.unity.com/). (Make sure to include the documentation, standard assets, Android build support, WebGL build support, and iOS build component)
1. Pull the source code from the [repo] (https://gitlab.com/ProjectAurora/AuroraApp.git)
2. Start Unity and select the "AuroraApp" folder in the Project open dialog
3. Select the "SignIn" scene and hit the play button

## Installation

#### Android:
Email staylornc@gmail.com to request access to the closed beta on the 
[Google Play Store](google-play-link)

#### iOS:
Email staylornc@gmail.com to request to be invited to join the TestFlight program

This link will only work once the app has been relased to production
[iTunes](https://itunes.apple.com/us/app/aurora-ally/id1241546853?ls=1&mt=8)

## Release History
<!--
* 0.2.1
    * CHANGE: Update docs (module code remains unchanged)
* 0.2.0
    * CHANGE: Remove `setDefaultXYZ()`
    * ADD: Add `init()`
* 0.1.1
    * FIX: Crash when calling `baz()` (Thanks @GenerousContributorName!)
* 0.1.0
    * The first proper release
    * CHANGE: Rename `foo()` to `bar()`
* 0.0.1
    * Work in progress
-->

## Meta

Sandra Taylor – staylornc@gmail.com

<!--Distributed under the XYZ license. See ``LICENSE`` for more information. -->

[https://gitlab.com/ProjectAurora/AuroraApp](https://gitlab.com/ProjectAurora/AuroraApp)

## Contributing

1. Fork the [Aurora App project](https://gitlab.com/ProjectAurora/AuroraApp).
2. Choose an [issue](https://gitlab.com/ProjectAurora/AuroraApp/issues) to work on. (You can find easy issues by looking at issues labeled "Accepting Merge Requests" and sorting issues by weight, low-weight issues will be the easiest to accomplish.)
3. Create your feature branch
4. Add the feature or fix the bug you’ve chosen to work on.
5. Commit your changes
4. Open a merge request. The earlier you open a merge request, the sooner you can get feedback. You can mark it as a Work in Progress to signal that you’re not done yet. <!--- Add tests and documentation if needed, as well as a changelog entry.--->
5. Wait for a reviewer. You’ll may need to change some things once the reviewer has completed a code review for your merge request. 
6. Get your changes merged!
 
Thank you for contributing!

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/yourname/yourproject/wiki
[google-play-link]: https://play.google.com/store/apps/details?id=com.openyourmindgames.aurora